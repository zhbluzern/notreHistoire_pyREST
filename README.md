# NotreHistoire / UnsereGeschichte / nossaIstorgia / lanostraStoria/ geneveMonde - python REST API Class

This is an easy to use Python Class to handle different REST-API endpoints for the various language projects of notreHistoire. 
For full information about all endpoints, different filter options or the structure of data objects for each endpoint of this API see the official [documentation](https://unseregeschichte.ch/api/v1)

# Installation

## Dependencies 
* Install Dependency python3-dotenv: `pip3 install python3-dotenv`

## Configure .env File
Create a file name `.env` in the same directory as the script `notreHistoireRest.py` is situated. 
Add the following lines in this file
```
ACCESS_TOKEN=YOUR_ACCESS_TOKEN
BASE_URL=YOUR_BASE_URL (e.g. https://unseregeschichte.ch)
API_URL=api/v1/
```

# Usage

## Initialization 

```python
import notreHistoireRest

# Initialize a NotreHistoire API-Session based on the given API-Key and API-URL in .env
nH = notreHistoireRest.notreHistoire()
```

## Get a User-ID by given username ([doc](https://unseregeschichte.ch/api/v1#users-collection-get))
```python
userName = "YOUR_USER"
userId = nH.getUserId(userName)
print(f"UserID of {userName}: {userId}")
```

## Get a list of Photos uploaded by a certain user (by User-ID) ([doc](https://unseregeschichte.ch/api/v1#media-photo-collection-get))
```python
filterParams = {"filter[user_id]": userId}
listOfPhotos = nH.listPhotosByUser(filterParams)
print(listOfPhotos)
```

## Get a list of Galleries created by a certain user (by User-ID) ([doc](https://unseregeschichte.ch/api/v1#galleries-collection-get))
```python
filterParams = {"filter[user_id]": userId}
listOfGalleries = nH.listGalleries(filterParams)
print(listOfGalleries)
```

## Get the list of entries for a certain Gallery ([doc](https://unseregeschichte.ch/api/v1#galleries-entries-get))
```python
filterParams = {}
listOfEntries = nH.listGalleryEntries(GalleryId,filterParams)
print(listOfEntries)
```

## Add an entry to a gallery ([doc](https://unseregeschichte.ch/api/v1#galleries-entries-put))
```python
nH.attachEntryToGallerie(EntryId, GalleryId)
```

## Remove an entry from a gallery ([doc](https://unseregeschichte.ch/api/v1#galleries-entries-delete))
```python
nH.removeEntryFromGallerie(EntryId, GalleryId)
```

## List Entries (e.g. by User-ID) ([doc](https://unseregeschichte.ch/api/v1#entries-collection-get))
```python
filterParams = {"filter[user_id]": userId}
listOfEntries = nH.listEntries(filterParams)
print(listOfEntries)
```

### Get the data of a certain entry
This method is also used to get the data of an existing entry (e.g. for a further update)
```python
filterParams = {"filter[id]": entryId}
listOfEntries = nH.listEntries(filterParams)
entry = listOfEntries["data"][0]
```

## Upload a new media ([doc](https://unseregeschichte.ch/api/v1#media-upload-post))
```python
mediaType = "photo" #choose one of the supported mediaTypes as given in the documentation
mediaFilePath = "" #absolute or realtive path including fileName.extension 
newMedia = nH.uploadMedia(mediaType, mediaFilePath)
```
## Create a new entry ([doc](https://unseregeschichte.ch/api/v1#entries-collection-post))
```python
#Sample Metadata-Dictionary for a photo, for further details see the documentation
metadata = {}
metadata["media_id"] = mediaId #Fetch the ID from the new created Media before
metadata["media_type"] = "photo" #choose one of the supported mediaTypes as given in the documentation
metadata["body"] = f"YOUR_Description"
metadata["title"] = "YOUR_title" 
metadata["type"] = "photo" #choose one of the supported mediaTypes as given in the documentation
metadata["status"] = "published" #see the documentations for further status
metadata["is_owner"] = 1 #see the documentation
metadata["is_owned"] = 0 #see the documentation
metadata["owner"] = "YOUR_OWNER"
metadata["is_commentable"] = 1 #see the documentation
metadata["license"] = "public_domain"  #choose one of the supported licenses as given in the documentation
metadata["periods[]"] = int(period_ID) #choose one of the created periods and input the ID as Integer
metadata["tags[]"] = ["YOUR_TAGS"]
metadata["is_author"] = 0 #see the documentation
metadata["author"] ="YOUR_Author_String"
newEntry = nH.createEntry(metadata)
```

## Update an entry ([doc](https://unseregeschichte.ch/api/v1#entries-collection-post))
```python
#Fetch the metadata of the existing media and update as necessary
metadata = {FETCHED_DICT_OF_MEDIA}
updateEntry = nH.updateEntry(entryId, metadata)
```

## Get the list of the periods available ([doc](https://unseregeschichte.ch/api/v1#periods-collection-get))
Adding entries to a certain period, it is necessary to know the specifi period-id, therefore you can parse with the following function a list of all periods availabe on your platform.
```python
listOfPeriods = nH.listPeriods()
print(listOfPeriods)
```