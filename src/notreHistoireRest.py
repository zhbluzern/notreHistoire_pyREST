import json
import requests
from dotenv import load_dotenv
import os

class notreHistoire:

    # Initialisiere notreHistoire-Klasse
    def __init__(self):
        load_dotenv()
        self.ACCESS_TOKEN = os.getenv("ACCESS_TOKEN")
        self.headers = {"Authorization": f"Bearer {self.ACCESS_TOKEN}","Accept":"application/json"} 
        self.baseUrl = os.getenv("BASE_URL")
        self.apiV = os.getenv("API_URL")
        self.apiUrl = f"{self.baseUrl}{self.apiV}"

    def listGalleryEntries(self, GalleryId, filterParams={}):
        filterParams.update({"per_page":50})
        print(filterParams)
        r = requests.get(f"{self.apiUrl}galleries/{GalleryId}/entries",params=filterParams, headers=self.headers)
        return (r.json())
    
    def listGalleries(self,filterParams={}):
        r = requests.get(f"{self.apiUrl}galleries",params=filterParams, headers=self.headers)
        return (r.json())
    
    def attachEntryToGallerie(self, EntryId, GalleryId):
        headers = self.headers 
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        r = requests.put(f"{self.apiUrl}galleries/{GalleryId}/entries", data=f"entry[]={int(EntryId)}", headers=headers)
        return (r.json())
    
    def listEntries(self, filterParams):
        r = requests.get(f"{self.apiUrl}entries", params=filterParams, headers=self.headers)
        print(f"{self.apiUrl}entries")
        print(filterParams)
        return r.json()
    
    def getEntry(self, EntryId, include=""):
        params = {}
        params["include"] = include
        r = requests.get(f"{self.apiUrl}entries/{EntryId}", params=params, headers=self.headers)
        return r.json()

    def removeEntryFromGallerie(self, EntryId, GalleryId):
        headers = self.headers 
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        r = requests.delete(f"{self.apiUrl}galleries/{GalleryId}/entries",data=f"entry[]={EntryId}", headers=headers)
        return r.json()
    
    def uploadMedia(self, mediaType, mediaFilePath):
        headers = {"Authorization": f"Bearer {self.ACCESS_TOKEN}"}
        data = {'type': mediaType}
        files = {'media': open(mediaFilePath, 'rb')}
        r = requests.post(f"{self.apiUrl}upload/", data=data, files=files, json=json.dumps(data), headers=headers)
        return r.json()
    
    def createEntry(self,metadata):
        headers = self.headers 
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        r = requests.post(f"{self.apiUrl}entries", data=metadata, headers=headers)
        return r.json()

    def getUserId(self,username):
        r = requests.get(f"{self.apiUrl}users/?filter[username]={username}", headers=self.headers)
        try:
            data = r.json()
            return data["data"][0]["id"]
        except:
            return None
        
    def listPhotosByUser(self,filterParams):
        r = requests.get(f"{self.apiUrl}photos", params=filterParams, headers=self.headers)
        return r.json()
    
    def updateEntry(self,entryId,metadata):
        headers = self.headers 
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        r = requests.put(f"{self.apiUrl}entries/{entryId}", data=metadata, headers=headers)
        return r.json()
    
    def listPeriods(self):
        r = requests.get(f"{self.apiUrl}periods", headers=self.headers)
        return r.json()